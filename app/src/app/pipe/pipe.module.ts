import { NgModule } from '@angular/core';
import { FormatErrorPipe } from './format-error.pipe';

@NgModule({
  declarations: [ FormatErrorPipe ],
  imports: [],
  exports: [ FormatErrorPipe ]
})
export class PipeModule { }
