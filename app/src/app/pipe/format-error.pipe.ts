import { Pipe, PipeTransform, SecurityContext } from '@angular/core';
import { DomSanitizer } from '@angular/platform-browser';

@Pipe({
    name: 'formatError'
})
export class FormatErrorPipe implements PipeTransform {
  constructor(
    private sanitizer: DomSanitizer
  ) { }

  transform(value: string, args?: any): string {

    let [, message] = value.split('.');
    const field = message.split(' ', 1)[0];
    message = message.split(field).join('');
    return this.sanitize(`<strong>${field}</strong> ${message}`);
  }

  sanitize(str) {
    return this.sanitizer.sanitize(SecurityContext.HTML, str);
  }
}
