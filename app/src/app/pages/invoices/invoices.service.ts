import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '@env/environment';

import { Invoice, InvoiceValidation } from './models/invoice.model';

@Injectable({
  providedIn: 'root'
})
export class InvoicesService {
  private apiBase: string = environment.apiUrl;
  constructor(
    private http: HttpClient) { }

  getInvoices(): Observable<Invoice[]> {
    return this.http.get<Invoice[]>(`${this.apiBase}/invoices`);
  }

  uploadInvoices(fileToUpload: File): Observable<Invoice[]> {
    const options = {
      headers: new HttpHeaders().append('Accept', 'text/csv')
    }

    const formData: FormData = new FormData();
    formData.append('invoices', fileToUpload, fileToUpload.name);
    const invoices = this.http.post<Invoice[]>(`${this.apiBase}/invoices/upload/csv`, formData, options)
      .pipe(map((x: any) => x.items));

    return invoices;
  }

  createInvoices(invoices: Invoice[], dryRun: boolean): Observable<InvoiceValidation> {
    const dryRunMode = dryRun ? `?dryRun=${dryRun}` : '';

    return this.http.post<InvoiceValidation>(`${this.apiBase}/invoices${dryRunMode}`, { items: invoices })
      .pipe(
        catchError((error: any) => {
          invoices.forEach(x => {
            x.valid = true;
            x.errors = [];
          });

          const result: InvoiceValidation = {
            valid: false
          };

          const errors = error.error.message;
          errors.forEach(item => {
            const positionAndField = item.split(' ', 1)[0];
            const [position, field] = positionAndField.split('.');
            invoices[position].valid = false;

            invoices[position].errors.push(item);
          });
          result.items = invoices;
          return of(result);
        })
      );
  }
}
