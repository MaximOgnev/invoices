export interface Invoice {
  id: number;
  invoiceId: number;
  dueDate: Date;
  amount: number;
  price: number;
  valid?: boolean;
  errors?: string[]
}

export interface InvoiceValidation {
  items?: Invoice[];
  valid?: boolean;
  saved?: boolean;
  messages?: string[]
}

export interface InvoiceErrorField {
  field: string;
  messages: string[];
}
