import { inject, TestBed } from '@angular/core/testing';

import { InvoicesService } from './invoices.service';
import { HttpTestingController, HttpClientTestingModule } from '@angular/common/http/testing';
import { environment } from '@env/environment';
import { Invoice } from './models/invoice.model';

describe('InvoicesService', () => {
  const apiURL = environment.apiUrl;
  let httpMock: HttpTestingController;
  let service: InvoicesService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [InvoicesService]
    });
  });

  beforeEach(inject([HttpTestingController, InvoicesService],
    (httpTestingController: HttpTestingController, invoicesService: InvoicesService) => {
      httpMock = httpTestingController;
      service = invoicesService;
    }));

  afterEach(() => {
    httpMock.verify();
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  it('should request a list of invoices', (done => {
    const invoicesMock = buildGetInvoicesMock();
    spyOn(service, 'getInvoices').and.callThrough();

    service.getInvoices().subscribe({
      next: invoices => {
        expect(service.getInvoices).toHaveBeenCalled();
        expect(invoices).toBeTruthy();
        expect(invoices.length).toBe(3);
        done();
      },
    });

    const req = httpMock.expectOne(request => request.url.startsWith(invoicesMock.url));

    expect(req.request.method).toEqual('GET');
    req.flush(invoicesMock.apiResponse);
  }));

  it('should post a list of invoices', (done => {
    const invoicesMock = buildPostInvoicesMock();
    spyOn(service, 'createInvoices').and.callThrough();

    service.createInvoices(invoicesMock.apiResponse.items, false).subscribe({
      next: response => {
        expect(service.createInvoices).toHaveBeenCalled();
        expect(response).toBeTruthy();
        expect(response.saved).toBe(true);
        expect(response.items.length).toBe(3);
        done();
      },
    });

    const req = httpMock.expectOne(request => request.url.startsWith(invoicesMock.url));

    expect(req.request.method).toEqual('POST');
    req.flush(invoicesMock.apiResponse);
  }));

 /**
   * Helper function to create expected values for all granted vectors
   */
  function buildGetInvoicesMock() {
    let result: {
      url: string;
      apiResponse: Invoice[];
    };

    result = {
      url: `${apiURL}/invoices`,
      apiResponse: [
          { id: 1, amount: 100, price: 100, invoiceId: 1, dueDate: new Date() },
          { id: 2, amount: 200, price: 100, invoiceId: 2, dueDate: new Date()},
          { id: 3, amount: 300, price: 100, invoiceId: 3, dueDate: new Date() }
      ],
    };

    return result;
  }

  function buildPostInvoicesMock() {
    let result: {
      url: string;
      apiResponse: {
        saved: boolean,
        items: Invoice[];
      };
    };

    result = {
      url: `${apiURL}/invoices`,
      apiResponse: {
        saved: true,
        items: [
          { id: 1, amount: 100, price: 100, invoiceId: 1, dueDate: new Date() },
          { id: 2, amount: 200, price: 100, invoiceId: 2, dueDate: new Date()},
          { id: 3, amount: 300, price: 100, invoiceId: 3, dueDate: new Date() }
        ],
      }
    };

    return result;
  }

});
