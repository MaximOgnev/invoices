import { NzMessageService } from 'ng-zorro-antd/message';
import { Observable, Subscription } from 'rxjs';

import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { InvoicesService } from './invoices.service';
import { Invoice, InvoiceValidation } from './models/invoice.model';

@Component({
  selector: 'app-invoices',
  templateUrl: './invoices.component.html',
  styleUrls: ['./invoices.component.scss']
})
export class InvoicesComponent implements OnInit, OnDestroy {

  private invoicesSubscription: Subscription;

  validateForm: FormGroup;
  fileToUpload: File = null;
  editRow: { [key: string]: { edit: boolean; data: Invoice } } = {};

  invoices$: Observable<Invoice[]>;
  invoices: Invoice[];

  isInvoicesUploaded = false;
  isInvoicesValid = false;

  constructor(
    private service: InvoicesService,
    private fb: FormBuilder,
    private message: NzMessageService
  ) { }

  import() {
    this.invoices$ = this.service.uploadInvoices(this.fileToUpload);
    this.invoicesSubscription = this.invoices$.subscribe(x =>  {
      this.invoices = x;
      this.isInvoicesUploaded = true;
      this.invoices.forEach(item => {
        item.valid = null;
        this.editRow[item.id] = {
          edit: false,
          data: { ...item }
        };
      });
    });
  }

  invoicesSave(dryRun: boolean) {
    this.service.createInvoices(this.invoices, dryRun).subscribe((x: InvoiceValidation) => {
      this.isInvoicesValid = x.valid;
      this.invoices = x.items;

      if (x.saved) {
        this.invoices = [];
        this.isInvoicesUploaded = false;
        this.isInvoicesValid = false;
        this.message.create('success', 'The data was saved in DB');
        return;
      }

      let index = 1;
      this.invoices.forEach(item => {
        item.id = index++;
        item.valid = this.isInvoicesValid ? true : item.valid;
        this.editRow[item.id] = {
          edit: false,
          data: { ...item }
        };
      });
    });
  }

  handleFileInput(files: FileList) {
    this.fileToUpload = files.item(0);
  }

  startEdit(id): void {
    this.editRow[id].edit = true;
  }

  cancelEdit(id): void {
    const index = this.invoices.findIndex(item => item.id === id);
    this.editRow[id] = {
      data: { ...this.invoices[index] },
      edit: false
    };
  }

  saveEdit(id): void {
    const index = this.invoices.findIndex(item => item.id === id);
    Object.assign(this.invoices[index], this.editRow[id].data);
    this.editRow[id].edit = false;
    this.invoices[index].valid = null;
    this.isInvoicesValid = false;
  }

  ngOnInit() {
    this.validateForm = this.fb.group({
      fileName: [null, [Validators.required]],
    });
  }

  ngOnDestroy() {
    if (this.invoicesSubscription) {
      this.invoicesSubscription.unsubscribe();
    }
  }

}
