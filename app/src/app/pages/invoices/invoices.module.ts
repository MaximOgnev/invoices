import { UIModule } from 'src/app/ui/ui.module';

import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { PipeModule } from '@app/pipe/pipe.module';

import { InvoicesRoutingModule } from './invoices-routing.module';
import { InvoicesComponent } from './invoices.component';

@NgModule({
  imports: [
    InvoicesRoutingModule,
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    UIModule,
    PipeModule
  ],
  declarations: [InvoicesComponent],
  exports: [InvoicesComponent]
})
export class InvoicesModule { }
