import { NzBadgeModule } from 'ng-zorro-antd/badge';
import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzFormModule } from 'ng-zorro-antd/form';
import { NzGridModule } from 'ng-zorro-antd/grid';
import { NzIconModule } from 'ng-zorro-antd/icon';
import { NzInputModule } from 'ng-zorro-antd/input';
import { NzLayoutModule } from 'ng-zorro-antd/layout';
import { NzMenuModule } from 'ng-zorro-antd/menu';
import { NzMessageModule } from 'ng-zorro-antd/message';
import { NzPopconfirmModule } from 'ng-zorro-antd/popconfirm';
import { NzPopoverModule } from 'ng-zorro-antd/popover';
import { NzTableModule } from 'ng-zorro-antd/table';
import { NzTagModule } from 'ng-zorro-antd/tag';
import { NzToolTipModule } from 'ng-zorro-antd/tooltip';

import { NgModule } from '@angular/core';

@NgModule({
  declarations: [],
  imports: [
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzInputModule,
    NzGridModule,
    NzTableModule,
    NzFormModule,
    NzPopconfirmModule,
    NzTagModule,
    NzIconModule,
    NzMessageModule,
    NzBadgeModule,
    NzToolTipModule,
    NzPopoverModule
  ],
  exports: [
    NzLayoutModule,
    NzMenuModule,
    NzButtonModule,
    NzInputModule,
    NzGridModule,
    NzTableModule,
    NzFormModule,
    NzPopconfirmModule,
    NzTagModule,
    NzIconModule,
    NzMessageModule,
    NzBadgeModule,
    NzToolTipModule,
    NzPopoverModule
  ]
})
export class UIModule { }
