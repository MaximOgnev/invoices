export default {
    passwordSaltRounds: process.env.NODE_ENV === 'production' ? 8 : 1,
    jwtSecret: process.env.JWT_SECRET || "JWT_SECRET"
};