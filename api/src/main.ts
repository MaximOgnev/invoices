import { Logger, ValidationPipe } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { useContainer } from 'class-validator';
import { AppModule } from './app.module';
import * as helmet from 'helmet';
import { JwtAuthGuard } from './modules/auth/auth.guard';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);
  
  const reflector = app.get('Reflector');
  const globalPrefix = 'api';
  app.setGlobalPrefix(globalPrefix);
  // DI for class validator
  useContainer(app.select(AppModule), { fallbackOnErrors: true });
  app.useGlobalPipes(new ValidationPipe({ skipMissingProperties: true, whitelist: true }));
  app.useGlobalGuards(new JwtAuthGuard(reflector));
  app.use(helmet());

  app.enableCors();

  if (process.env.NODE_ENV !== 'production') {
    const options = new DocumentBuilder()
      .setTitle('Distillery')
      .setDescription('Distillery API')
      .setVersion(process.env.npm_package_version)
      .setBasePath(globalPrefix)
      //.addTag('invoices')
      .addServer('http://')
      .addServer('https://')
      .build();
    const document = SwaggerModule.createDocument(app, options);
    SwaggerModule.setup('docs', app, document);
  }
  
  const port = process.env.PORT || 3333;
  await app.listen(port, () => {
    Logger.log('Listening at http://localhost:' + port + '/' + globalPrefix);
  });
}
bootstrap();
