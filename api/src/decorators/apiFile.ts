import { ApiBody } from '@nestjs/swagger';

export const ApiFile = (fileName: string = 'file', description: string = ""): MethodDecorator => (
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor,
) => {
  ApiBody({
    schema: {
      type: 'object',
      properties: {
        [fileName]: {
          type: 'string',
          format: 'binary',
        },
      },
    },
    description
  })(target, propertyKey, descriptor);
};
