import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDate,  IsInt, IsNumber } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Invoice { 

    @PrimaryGeneratedColumn()
    id: number;

    @Type(() => Number)
    @IsInt()
    @Column()
    @ApiProperty({ required: true, type: Number })
    invoiceId:number;

    @Type(() => Number)
    @IsNumber()
    @Column({ type: "float" })
    @ApiProperty({ required: true, type: Number })
    amount:number;

    @Type(() => Date)
    @IsDate()
    @Column()
    @ApiProperty({ required: true, type: Date })
    dueDate:Date;

    @Type(() => Number)
    @IsNumber()
    @Column({ type: "float" })
    @ApiProperty({ required: true, type: Number })
    price:number;
}

