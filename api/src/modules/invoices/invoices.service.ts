import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm/dist/common/typeorm.decorators';
import { Invoice } from './invoice.entity';
import { CsvParser } from 'nest-csv-parser'
import { Repository } from 'typeorm';
import * as fs  from 'fs';

@Injectable()
export class InvoicesService  {
    constructor(
        @InjectRepository(Invoice) 
        private invoicesRepository: Repository<Invoice>,
        private readonly csvParser: CsvParser) { }

    async getInvoices(): Promise<Invoice[]> {
        return await this.invoicesRepository.find();
    }

    async getInvoice(_id: number): Promise<Invoice[]> {
        return await this.invoicesRepository.find({
            where: [{ "id": _id }]
        });
    }

    async bulkCreate(invoices: Invoice[]) {
        await this.invoicesRepository
            .createQueryBuilder()
            .insert()
            .values(invoices)
            .execute(); 
    }

    async parseInvoicesFile(fileName: string, separator) {
        
        const csvConfig = { separator, headers: ['invoiceId', 'amount', 'dueDate'] };
        const stream = fs.createReadStream(fileName)
        const invoices: Invoice[] = (await this.csvParser.parse(stream, Invoice, null, null, csvConfig)).list;
        invoices.forEach((inv, index) => {
            inv.id = index;
            inv.price = inv.amount * 3;
        });
        return invoices;
      }
}
