import { Body, Controller, Get, Param, Post, Query, UploadedFile, UseInterceptors } from '@nestjs/common';
import { ApiOperation, ApiOkResponse, ApiConsumes, ApiTags } from '@nestjs/swagger';
import { ApiFile } from 'src/decorators/apiFile';
import { InvoicesFileInterceptor } from 'src/interceptors/invoicesFileInterceptor';
import { InvoiceListDto } from './invoice.dto';
import { InvoicesService } from './invoices.service';

@ApiTags('invoices')
@Controller('invoices')
export class InvoicesController {
    constructor(private service: InvoicesService) { }

    @ApiOperation({
      summary: 'Get invoice by Id',
    })
    @ApiOkResponse()
    @Get(':id')
    get(@Param() params) {
        return this.service.getInvoice(params.id);
    }

    @ApiOperation({
      summary: 'Get all invoices',
    })
    @ApiOkResponse()
    @Get()
    getAll() {
        return this.service.getInvoices();
    }

    @ApiOperation({
        summary: 'Import invoices',
        description: 'Import invoices file to server',
    })
    @ApiOkResponse({ type: InvoiceListDto })
    @ApiConsumes('multipart/form-data')
    @ApiFile('invoices', 'Invoices file csv type')
    @UseInterceptors(InvoicesFileInterceptor)
    @Post('upload/csv')
    async uploadResumeFile(@UploadedFile() file, @Query() query) {
      const delimiter = query.delimiter || ',';
      let fileName = __dirname + "/../assets/uploads/" + file.filename;
  
      const invoices = await this.service.parseInvoicesFile(fileName, delimiter);

      return {
        items: invoices
      };
    }

    @ApiOperation({
        summary: 'Create invoices',
    })
    @ApiOkResponse()
    @Post()
    async bulkCreate(@Body() payload: InvoiceListDto, @Query() query) {
      if (query.dryRun) {
        return {
          valid: true,
          items: payload.items
        };
      }

      await this.service.bulkCreate(payload.items);
      return {
        saved: true,
        items: payload.items
      };
    }

}
