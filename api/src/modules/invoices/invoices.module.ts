import { Module } from '@nestjs/common';
import { InvoicesService } from './invoices.service';
import { InvoicesController } from './invoices.controller';
import { Invoice } from './invoice.entity';
import { TypeOrmModule } from '@nestjs/typeorm/dist/typeorm.module';
import { CsvModule } from 'nest-csv-parser'

@Module({
  imports: [
    TypeOrmModule.forFeature([Invoice]), 
    CsvModule
  ],
  providers: [InvoicesService],
  controllers: [InvoicesController]
})
export class InvoicesModule {}
