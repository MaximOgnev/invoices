import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import { IsDefined, IsNotEmpty, IsArray, ArrayNotEmpty, ArrayMinSize, ValidateNested } from 'class-validator';
import { Invoice } from "./invoice.entity";

export class InvoiceListDto {
    @ApiProperty({ required: true, type: [Invoice], description: 'Invoices list' })
    @IsDefined()
    @IsNotEmpty()
    @IsArray()
    @ArrayNotEmpty()
    @ArrayMinSize(1)
    @ValidateNested()
    @Type(() => Invoice)
    items: Invoice[];
}
  