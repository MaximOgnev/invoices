import { createParamDecorator, SetMetadata } from '@nestjs/common';

export const IS_PUBLIC_ROUTER = 'router:Public';
export const Public = () => SetMetadata(IS_PUBLIC_ROUTER, true);

export const CurrentUser = createParamDecorator((_, ctx) => { 
    const request = ctx.switchToHttp().getRequest();
    return request.user
});