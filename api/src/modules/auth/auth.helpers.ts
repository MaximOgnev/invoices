import * as bcrypt from 'bcrypt';
import { generalConfig } from '../../config';

export const IsPasswordCorrect = async (password: string, hash: string) => {
    return await bcrypt.compare(password, hash);
};
  
export const HashPassword = async (password: string) => {
    return await bcrypt.hash(password, generalConfig.passwordSaltRounds);
};