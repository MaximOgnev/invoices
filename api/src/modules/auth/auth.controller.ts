import { Controller, HttpStatus, Post, Request, UseGuards } from '@nestjs/common';
import {
    ApiOkResponse, ApiOperation, ApiTags, ApiUnauthorizedResponse
} from '@nestjs/swagger';

import { User } from '../users/user.entity';
import { Public } from './auth.decorator';
import { LocalAuthGuard } from './auth.guard';
import { AuthService } from './auth.service';

@ApiTags('Auth')
@Controller('auth')
export class AuthController {
    constructor(private authService: AuthService) {}
    
    @ApiOperation({ summary: 'Sign in', description: 'Sign in via credentials' })
    @ApiUnauthorizedResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Invalid email or password' })
    @ApiOkResponse({ type: User })
    @Public()
    @UseGuards(LocalAuthGuard)
    @Post('login')
    async login(@Request() req) {
      return this.authService.login(req.user);
    }
}
