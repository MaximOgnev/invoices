import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { User } from '../users/user.entity';
import { UsersService } from '../users/users.service';
import { HashPassword, IsPasswordCorrect } from './auth.helpers';

@Injectable()
export class AuthService {
    constructor(
        private usersService: UsersService, 
        private jwtService: JwtService
        ) {}

    public async validateUser(username: string, password: string): Promise<User | undefined> {
        const user = await this.usersService.findByUsername(username);

        if (user && await IsPasswordCorrect(password, user.password)) {
            return user;
        }
        return null;
    }

    public async login(user: User) {
        const payload = { username: user.username, id: user.id };
        return {
          access_token: this.jwtService.sign(payload),
        };
    }
}
