import { ApiProperty } from '@nestjs/swagger';
import { Exclude, Type } from 'class-transformer';
import { IsDate,  IsDefined,  IsInt, IsNumber, IsString, Length } from 'class-validator';
import { Entity, Column, PrimaryGeneratedColumn, Index } from 'typeorm';

@Entity()
export class User { 

    @ApiProperty({ readOnly: true, type: 'number' })
    @PrimaryGeneratedColumn()
    id: number;

    @ApiProperty({ type: 'string' })
    @IsDefined()
    @IsString()
    @Length(4, 25)
    @Index({ unique: true })
    @Column()
    public username: string;

    @Exclude({ toPlainOnly: true })
    @IsString()
    @Length(6, 256)
    @Column()
    public password: string;
}

