import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import { User } from './user.entity';

@Injectable()
export class UsersService {
    constructor(
        @InjectRepository(User) 
        private usersRepository: Repository<User>) { }

     /**
     * Find user by username
     * @param {string} username
     * @returns {Promise<User>} User
     * @memberof UsersService
     */
    public async findByUsername(username: string): Promise<User | undefined> {
        return await this.usersRepository.findOne({ username });
    }
}
