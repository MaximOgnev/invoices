import { Controller, Get, HttpStatus } from '@nestjs/common';
import { ApiOperation, ApiUnauthorizedResponse, ApiOkResponse, ApiTags } from '@nestjs/swagger';
import { CurrentUser } from '../auth/auth.decorator';
import { User } from './user.entity';

@ApiTags('Users')
@Controller('users')
export class UsersController {
    @ApiOperation({ summary: 'Get profile' })
    @ApiUnauthorizedResponse({ status: HttpStatus.UNAUTHORIZED, description: 'Invalid JWT token' })
    @ApiOkResponse({ type: User })
    @Get('profile')
    getProfile(@CurrentUser() user) {
        return user;
    }
}
