import { BadRequestException } from '@nestjs/common';
import { diskStorage } from 'multer';
import { FileInterceptor } from '@nestjs/platform-express';
import { extname } from 'path';

const editFileName = (req, file, callback) => {
  const name = file.originalname.split('.')[0];
  const fileExtName = extname(file.originalname);
  const randomName = Array(4)
    .fill(null)
    .map(() => Math.round(Math.random() * 16).toString(16))
    .join('');
  callback(null, `${name}-${randomName}${fileExtName}`);
};

export const InvoicesFileInterceptor = FileInterceptor('invoices', {
  storage: diskStorage({
    destination: __dirname + '/../assets/uploads',
    filename: editFileName,
  }),
  fileFilter: (req: any, file: any, cb: any) => {
    if (file.mimetype.includes("csv") || file.mimetype.includes("excel")) {
      cb(null, true);
    } else {
      cb(new BadRequestException("Please upload only csv file."), false);
    }
  },
});
