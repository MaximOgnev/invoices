## Demo App

You should have installed [My SQL](https://www.mysql.com/downloads/)

Watch a small [demo](https://drive.google.com/file/d/1KKOSkdyKzuzly3Gfs8tIRYici0yyewK0/view)

FrontEnd: [Angular 10](https://angular.io/) + [NG-ZORRO](https://ng.ant.design/)

BackEnd: [NestJs](https://nestjs.com/) + [TypeORM](https://typeorm.io/#/)

The API documentation [here](http://localhost:3000/docs)

## Install dependencies

We used npm ci for installing dependencies
```bash
$ npm ci
```

## Running the Frontend (Angular + )

```bash
# development
$ ng serve
```

## Running the Backend

You need to update .env file and create a new DB. TypeORM uses Sync mode - the tables will be restored automatically.

```bash
# development
$ npm run start
```